/******************************************************************************
* |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
*                          ..-.:.:...
*                       :.-- -     ..:...
*                   :.:. -             -.:...
*               :.:. -                     ..:...
*           :.:. .            _;:__.          ...-...
*       :.:. .               :;    -+_    -|       .--...
*   :.-- .                    -=      -~-.-           -.:...
* -:...              ___.      -=_                        -.--
* ...    .          =;  --=_     :=                   ..   ...
* .-.      . .              ~-___=;               . -      .:.
* ...           -.                             -.          ...
* .:.                .                    . .              .:.
* ...                   -.             -.                  ...
* .:.                      ...    . -                -~4>  .:.
* ...       _^+_.              -.                       2  ...
* .:.           ~,              .                 /'   _(  .:.
* ....           <              -          +'  ^LJ>   _^   ...
* .:..          _);             -     _   J   _/  ~~-'    .:.
* ....        _&i^i             .   _~_, <(   .^           ...
*  :.       _v>^  <             .  _X~'  -s,               .:.
*  :.             -=            .   S      ^'              ...
*  :....           -=_  ,       .   2                    ..-.:
*     :....          -^^        .                    .-.:. .
*        ..:...                 .                 ..:. -
*            -.:...             .           . :.-- -
*                 :....         .         -.-- .
*                    -.:...     .    ..:.: -
*                         -.:......--. -
*                             -.:. .
* 
* Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
* http://wide.io/
* ----------------------------------------------------------------------
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License.
*     
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*     
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/
* This work is released under GPL v 3.0
* ----------------------------------------------------------------------
* For all information : copyrights@wide.io
* ----------------------------------------------------------------------
* 
* |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
******************************************************************************/
function get_field{{widgetuuid}}(p) {
    $("#body_ajax{{widgetuuid}}").html('loading...<a href="{{object_class.get_add_url}}?_AJAX=1"> {{object_class.get_add_url}}?_AJAX=1</a>');
    if (p.pme) {
        $("#body_ajax{{widgetuuid}}").load("{{object_class.get_add_url}}?" + $.param({ '_AJAX': '1', 'ahah_container': 'body_ajax{{widgetuuid}}', 'on_success': 'closeDialog{{widgetuuid}} ();' }));
    } else {
        $("#body_ajax{{widgetuuid}}").load("{{object_class.get_add_url}}?" + $.param({ '_AJAX': '1', 'ahah_container': 'body_ajax{{widgetuuid}}', 'on_success': 'closeDialog{{widgetuuid}} ();' }), function () {
            $(this).find(".fieldWrapper > a[data-toggle='modal']").remove();
        });
    }
}
;
function openDialog{{widgetuuid}}(el) {
    $('#modal{{widgetuuid}}').modal('show');
    previous_modal = $(el.target).closest('.modal-body-relative');
    previous_modal_exists = previous_modal.length ? true : false;
    var p = {pme: previous_modal_exists};
    get_field{{widgetuuid}}(p);
}
;
function closeDialog{{widgetuuid}} () {
    $('#modal{{widgetuuid}}').modal('hide');
}
;
$('.modal-header > button.close').click(function (e) {
    $(e.target).closest('.modal.modal-relative').modal('hide');
});
$('.modal.modal-relative:not(.bound)').addClass('bound').on('shown.bs.modal', function (e) {
    target = $(e.target).find('textarea, input');
    target.on('focus', function () {
        $(e.target).animate({ scrollTop: target.closest('.fieldWrapper').offset().top }, 500);
    });
    e.stopPropagation();
});
$('.modal.modal-relative:not(.bound)').addClass('bound').on('hidden.bs.modal', function (e) {
    e.stopPropagation();
});
$('.modal-body-relative:not(.bound)').addClass('bound').on('shown.bs.modal', function (e) {
    $(this).css('overflow-y', 'scroll');
});
var default_value = "{{ value }}";
function refresh_select{{widgetuuid}}() {
    $.ajax("{{object_class.get_list_url}}?_AJAX=1&_JSON=2&PERPAGE=1000&is_draft=bool_false", { dataType: "json", success: function (elems, ts, jqXHR) {
        r = ""; {% if not required %} r = '<i> No {{object_class.ctrname_p}} available </i>';
            $("#select{{widgetuuid}}").replaceWith(r); {% else %}
            for (i = 0; i < elems.length; i++) {
                r += '<option value="' + elems[i][0] + '"';
                if (elems[i][0] == default_value) r += ' selected';
                r += '>' + elems[i][1] + '</option>';
            } $("#select{{widgetuuid}}").text(r); {% endif %} } });
}
$(document).ready(function () {
    $("#modal{{widgetuuid}}").on('hidden', function () {
        refresh_select{{widgetuuid}}();
    });
    refresh_select{{widgetuuid}}();
});