/******************************************************************************
* |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
*                          ..-.:.:...
*                       :.-- -     ..:...
*                   :.:. -             -.:...
*               :.:. -                     ..:...
*           :.:. .            _;:__.          ...-...
*       :.:. .               :;    -+_    -|       .--...
*   :.-- .                    -=      -~-.-           -.:...
* -:...              ___.      -=_                        -.--
* ...    .          =;  --=_     :=                   ..   ...
* .-.      . .              ~-___=;               . -      .:.
* ...           -.                             -.          ...
* .:.                .                    . .              .:.
* ...                   -.             -.                  ...
* .:.                      ...    . -                -~4>  .:.
* ...       _^+_.              -.                       2  ...
* .:.           ~,              .                 /'   _(  .:.
* ....           <              -          +'  ^LJ>   _^   ...
* .:..          _);             -     _   J   _/  ~~-'    .:.
* ....        _&i^i             .   _~_, <(   .^           ...
*  :.       _v>^  <             .  _X~'  -s,               .:.
*  :.             -=            .   S      ^'              ...
*  :....           -=_  ,       .   2                    ..-.:
*     :....          -^^        .                    .-.:. .
*        ..:...                 .                 ..:. -
*            -.:...             .           . :.-- -
*                 :....         .         -.-- .
*                    -.:...     .    ..:.: -
*                         -.:......--. -
*                             -.:. .
* 
* Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
* http://wide.io/
* ----------------------------------------------------------------------
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License.
*     
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*     
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/
* This work is released under GPL v 3.0
* ----------------------------------------------------------------------
* For all information : copyrights@wide.io
* ----------------------------------------------------------------------
* 
* |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
******************************************************************************/
function get_field() {
    $("#body_ajax{{widgetuuid}}").html('loading...<a href="{{object_class.get_add_url}}?_AJAX=1"> /data/{{object_class.get_add_url}}/add/?_AJAX=1</a>');
    $("#body_ajax{{widgetuuid}}").load("{{object_class.get_add_url}}?" + $.param({'_AJAX': '1', 'ahah_container': 'body_ajax{{widgetuuid}}', 'on_success': 'closeDialog{{widgetuuid}} ();' }));
}
;
function openDialog{{widgetuuid}}() {
    $('#modal{{widgetuuid}}').modal('show');
    get_field();
}
;
function closeDialog{{widgetuuid}} () {
    $('#modal{{widgetuuid}}').modal('hide');
}
;
function refresh_list{{widgetuuid}}() {
    $("#list_select{{widgetuuid}}").load("{{object_class.get_list_url}}?_AJAX=1&is_draft=bool_false&mode=select&formuuid={{widgetuuid}}&widgetuuid={{widgetuuid}}&add_enabled=0");
}
$(document).ready(function () {
    $("#modal{{widgetuuid}}").on('hidden', function () {
        refresh_list{{widgetuuid}}();
    });
    refresh_list{{widgetuuid}}();
    refresh_selection{{widgetuuid}}();
});

function add_to_selection{{widgetuuid}}(elemid) {
    var eid = $("selection{{widgetuuid}}");
    eid.val((eid.val().split(",").concat([elemid])).join(","));
    refresh_selection{{widgetuuid}}();
}
function remove_from_selection{{widgetuuid}}(elemid) {
    var eid = $("selection{{widgetuuid}}");
    eid.val((eid.val().split(",").filter(function (x) {
        return x != elemid;
    })).join(","));
    refresh_selection{{widgetuuid}}();
}
function refesh_selection{{widgetuuid}}() {
    var eid = $("selection{{widgetuuid}}").val().split(",");
    $("selectionview{{widgetuuid}}").html(eid.length.toString() + " objects selected");
}
