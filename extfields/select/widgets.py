# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import uuid
import json
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe

import  settings


def getuuid():
    return str(uuid.uuid1())


class SelectWidget(Widget):

    def __init__(
            self, attrs={}, model=None, field_display='name', *args, **kwargs):
        self.attrs = attrs
        self.model = model
        self.field_display = field_display

    def render(self, name, value, attrs=None):
        print "Select Widget value is", value
        widgettemplate = loader.get_template('extfields/select.html')
        # 'NoneType' object has no attribute 'objects'
        object_class = self.model
        html_content = widgettemplate.render(
            Context(
                {
                    'name': name,
                    'value': value,
                    'widgetuuid': getuuid().replace(
                        '-',
                        'P'),
                    'object_class': object_class,
                    'objects': object_class.objects.all(),
                    'field_display': self.field_display,
                    'MEDIA_URL': settings.MEDIA_URL}))
        return mark_safe(html_content)

    def value_from_datadict(self, data, files, name):
        return data[name] if name in data else None


class DynamicSelectWidget(Widget):

    def __init__(self, attrs={}, model=None, default=None,
                 required=False, *args, **kwargs):
        # print attrs
        self.attrs = attrs
        self.model = model
        self.default = default
        self.required = required
        # if (type(model) == str):
        #from django.db.models.loading import get_model
        # model=get_model(*model.split('.',1))
        # self.model=model
        # self.attrs=attrs
        # self.choices=choices
        #self.selected=selected or 0
        # self.element_display_filter=element_display_filter
        self.request = None
        self.instance=None

    def set_request(self, r):
        self.request = r

    def set_instance(self, i):
        self.instance = i

    def render(self, name, value, attrs=None):
        if value is None:
            value = self.default
        widgettemplate = loader.get_template('extfields/dynamic_select.html')
        object_class = self.model
        list_filters = {}
        widget_add_unknown = hasattr(self.model, 'widget_add_unknown')
        if widget_add_unknown:
            info = self.model.widget_add_unknown
        context = {
            'name': name,
            'value': value,
            'required': self.required,
            'widgetuuid': getuuid().replace('-', 'Q'),
            'object_class': object_class,
            'objectlist': self.choices,
            'invoke_extra':self.request.REQUEST.get("_INVOKE_EXTRA_"+name.upper(),""),
            'add_unknown': "1" if (hasattr(self.model, "invoke_by_name") or widget_add_unknown) else "0",
            'list_filters': json.dumps(list_filters),
            'MEDIA_URL': settings.MEDIA_URL
        }
        html_content = widgettemplate.render(Context(context))
        return mark_safe(html_content)

    def value_from_datadict(self, data, files, name):
        return data[name] if name in data else None


class DynamicListSelectWidget(Widget):

    def __init__(self, attrs={}, model=None, *args, **kwargs):
        self.attrs = attrs
        self.model = model

    def render(self, name, value, attrs=None):
        # from wioframework.uuid import getuuid
        widgettemplate = loader.get_template(
            'extfields/dynamic_list_select.html')
        object_class = self.model
        html_content = widgettemplate.render(
            Context(
                {
                    'name': name,
                    'value': value,
                    'widgetuuid': getuuid(),
                    'object_class': object_class,
                    'objectlist': self.choices,
                    'MEDIA_URL': settings.MEDIA_URL}))
        return mark_safe(html_content)

    def value_from_datadict(self, data, files, name):
        return data[name] if name in data else None


class MultipleSelectWidget(Widget):

    def __init__(
            self, attrs={}, model=None, field_display='name', *args, **kwargs):
        self.attrs = attrs
        self.model = model
        self.field_display = field_display
        self.request = None

    def set_request(self, r):
        self.request = r

    def render(self, name, value, attrs=None):
        # rint "Value is",value
        widgettemplate = loader.get_template(
            'extfields/multipleSelectList.html')
        # 'NoneType' object has no attribute 'objects'
        object_class = self.model
        lvkwargs = {  # 'html_transform': lambda x: "<div class='multiple-select-checkbox'><input type=\"checkbox\" /> %s</div>"%(x,),
            'perpage': 5
        }
        html_content = widgettemplate.render(Context({'name': name,
                                                      'value': value,
                                                      'object_class': object_class,
                                                      'field_display': self.field_display,
                                                      'lvkwargs': lvkwargs,
                                                      'MEDIA_URL': settings.MEDIA_URL,
                                                      'request': self.request}))
        return mark_safe(html_content)

    def value_from_datadict(self, data, files, name):
        if "_orig_data" in data:
          return data["_orig_data"].getlist(name)
        else: 
          return None  

class SubElementsWidget(Widget):
    """
    When elements are created specifically for specific element + field
    """
    def __init__(
            self, attrs={}, model=None, field_display='name', *args, **kwargs):
        self.attrs = attrs
        self.model = model
        self.field_display = field_display
        self.request = None
        self.instance=None

    def set_request(self, r):
        self.request = r

    def set_instance(self, i):
        self.instance = i


    def render(self, name, value, attrs=None):
        # rint "Value is",value
        widgettemplate = loader.get_template(
            'extfields/subElementsList.html')
        # 'NoneType' object has no attribute 'objects'
        object_class = self.model
        lvkwargs = {  # 'html_transform': lambda x: "<div class='multiple-select-checkbox'><input type=\"checkbox\" /> %s</div>"%(x,),
            'perpage': 10
        }
        html_content = widgettemplate.render(Context({'name': name,
                                                      'x':self.instance,
                                                      'value': value,
                                                      'object_class': object_class,
                                                      'field_display': self.field_display,
                                                      'lvkwargs': lvkwargs,
                                                      'MEDIA_URL': settings.MEDIA_URL,
                                                      'widgetuuid': str(uuid.uuid1()).replace('-',''),
                                                      'request': self.request}))
        return mark_safe(html_content)

    def value_from_datadict(self, data, files, name):
        if "_orig_data" in data:
          return data["_orig_data"].getlist(name)
        else: 
          return None  

